EXPENSE REIMBURSEMENT SYSTEM (ERS)
-------------------------------------------------------------------------------------

Project Description
-------------------
The Expense Reimbursement System (ERS) will manage the process of reimbursing employees for expenses incurred while on company time. All employees in the company can login and submit requests for reimbursement and view their past tickets and pending requests. Finance managers can log in and view all reimbursement requests and past history for all employees in the company. Finance managers are authorized to approve and deny requests for expense reimbursement.

Technologies Used
-----------------
- Java - 1.8.0
- JavaScript - 1.8
- HTML/CSS
- MariaDB
- Selenium
- JUnit 4
- Mockito
- Javalin

Features
--------
- Users are able to log in and log out.
- Employees are able to submit reimbursement requests and view their requests.
- Financial Manager Users are able to view all reimbursement requests, filter by 
  reimbursement status, approve/deny requests.
- Reimbursement requests are stored in an AWS RDS database.

Getting Started
---------------
- git command to clone the repository:
    git clone https://gitlab.com/RyanCurley/employee-reimbursement-system-ers.git
- Using your IDE of choice, run MainDriver.class
- Navigate on your browser of choice to http://localhost:9100/html/login.html
- Employee Credentials: username: ryan, password: pass
- Financial Manager: username: charlie, password: pass
