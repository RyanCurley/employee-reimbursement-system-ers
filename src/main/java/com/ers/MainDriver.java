package com.ers;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.ers.controller.ReimbursementController;
import com.ers.controller.UserController;
import com.ers.dao.ReimbursementDBConnection;
import com.ers.dao.ReimbursementDao;
import com.ers.dao.UserDao;
import com.ers.service.ReimbursementService;
import com.ers.service.UserService;

import io.javalin.Javalin;

public class MainDriver {

	public final static Logger log = Logger.getLogger(MainDriver.class);
	
	public static void main(String[] args) {
		
		log.setLevel(Level.ALL);
		
		UserController uCon = new UserController(new UserService(new UserDao(new ReimbursementDBConnection())));
		ReimbursementController rCon = new ReimbursementController(new ReimbursementService(new ReimbursementDao(new ReimbursementDBConnection())));
		
		Javalin app = Javalin.create(config ->{
			config.addStaticFiles("/frontend");
		});
		
		app.start(9100);
		
		app.post("/users/login", uCon.postLogin);
		app.get("/users/session", uCon.getSessUser);
		
		app.post("/users/logout", uCon.postLogout);
		app.get("/users/:u_id/reimbursements", rCon.getReimbByUser);
		
		app.get("/reimbursements/:r_id/view", rCon.reimbGetId);
		app.get("/reimbursements/all", rCon.reimbGetAll);
		app.post("/reimbursements/create", rCon.postReimbursement);
		app.put("/reimbursements/:r_id/approve", rCon.putReimbursementApprove);
		app.put("/reimbursements/:r_id/deny", rCon.putReimbursementDeny);
		app.get("/reimbursements/approval/:s_id", rCon.getReimbsByStatus);
		app.put("/reimbursements/:r_id/resolve", rCon.putReimbursementResolve);
		
		app.exception(NullPointerException.class, (e, ctx) ->{
			ctx.status(404);
			ctx.result("user does not exist!");
		});
	}

}
