package com.ers.service;

import com.ers.dao.UserDao;
import com.ers.model.User;

public class UserService {
	
	private UserDao uDao;
	
	public User getUser(String username) {
		User user = uDao.findByUsername(username);
		if(user == null) {
			throw new NullPointerException();
		}
		return user;
	}
	
	public boolean userVerify(String username, String password) {
		boolean isVerified = false;
		User user = getUser(username);
		if(user.getPassword().equals(password)) {
			isVerified=true;
		}
		return isVerified;
	}
	
	public void postUser(int u_role, String username, String password, String fname, String lname, String email) {
		uDao.insertUser(u_role, username, password, fname, lname, email);
	}
	
	public UserService() {
		
	}
	
	public UserService(UserDao uDao) {
		super();
		this.uDao = uDao;
	}
}
