package com.ers.service;

import java.util.List;

import com.ers.dao.ReimbursementDao;
import com.ers.model.Reimbursement;

public class ReimbursementService {
	
	private ReimbursementDao rDao;
	
	public Reimbursement getReimbursement(int reimb_id) {
		Reimbursement reimb = rDao.findById(reimb_id);
		if(reimb == null) {
			throw new NullPointerException();
		}
		return reimb;
	}
	
	public List<Reimbursement> getAllReimbursements(){
		List<Reimbursement> reimbList = rDao.getAllReimb();
		if(reimbList.isEmpty()) {
			
		}
		return reimbList;
	}
	
	public List<Reimbursement> getReimbByUser(int userId){
		List<Reimbursement> reimbList = rDao.getAllReimbByUser(userId);
		 
		return reimbList;
	}
	
	public List<Reimbursement> getReimbsByStatus(int reimbStatus){
		List<Reimbursement> reimbList = rDao.getAllReimbByStatus(reimbStatus);
		return reimbList;
	}
	
	public void createReimb(int amount, int authorId, String reimbDesc, int reimbType) {
		rDao.insertReimbursement(amount, authorId, reimbDesc, reimbType);
		System.out.println("reimb successfully created");
	}
	
	public void approveReimb(int reimbId) {
		rDao.approveReimb(reimbId);
		System.out.println("reimb successfully approved");
	}
	
	public void denyReimb(int reimbId) {
		rDao.denyReimb(reimbId);
		System.out.println("reimb successfully denied");
	}
	
	public void resolveReimb(int reimbId, int resolverId) {
		rDao.resolveReimb(reimbId, resolverId);
		System.out.println("reimb successfully resolved!");
	}
	
	public ReimbursementService() {
		
	}
	
	public ReimbursementService(ReimbursementDao rDao) {
		this.rDao = rDao;
	}
	
}
