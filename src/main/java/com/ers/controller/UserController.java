package com.ers.controller;

import org.apache.log4j.Logger;

import com.ers.model.User;
import com.ers.service.UserService;
import io.javalin.http.Handler;

public class UserController {
	
	public static final Logger log = Logger.getLogger(UserController.class);
	
	private UserService uServe;
	
	public Handler postLogin = (ctx) ->{
		
		if(uServe.userVerify(ctx.formParam("username"), ctx.formParam("password"))) {
			System.out.println("User Verified!");
			ctx.sessionAttribute("user", uServe.getUser(ctx.formParam("username")));
			ctx.redirect("/html/home.html");
			log.warn("User Successfully Logged In");
		} else {
			System.out.println("User Not Verified!");
			ctx.redirect("/html/badlogin.html");
			log.error("User Failed to LogIn");
		}
	};
	
	public Handler getSessUser = (ctx) ->{
		if(ctx.sessionAttribute("user")!=null) {
			User user = (User)(ctx.sessionAttribute("user"));
			ctx.json(user);
		}else {
			System.out.println("problem!");
			ctx.redirect("/html/login.html");
		}
	};
	
	public Handler postLogout = (ctx) ->{
		System.out.println("user logged out");
		ctx.req.getSession().invalidate();
		ctx.redirect("/html/logout.html");
	};
	
	public Handler postUser = (ctx) ->{
		uServe.postUser(Integer.parseInt(ctx.formParam("role")), ctx.formParam("username"), ctx.formParam("password"), ctx.formParam("fname"), ctx.formParam("lname"), ctx.formParam("email"));
		ctx.redirect("/html/home.html");
	};
	
	public UserController() {
		
	}
	
	public UserController(UserService uServe) {
		super();
		this.uServe = uServe;
	}
	
}
