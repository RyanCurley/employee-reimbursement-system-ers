package com.ers.controller;

import java.util.List;

import org.apache.log4j.Logger;

import com.ers.model.Reimbursement;
import com.ers.model.User;
import com.ers.service.ReimbursementService;
import io.javalin.http.Handler;

public class ReimbursementController {
	
	public static final Logger log = Logger.getLogger(ReimbursementController.class);
	
	private ReimbursementService rServe;
	
	public Handler reimbGetId = (ctx) ->{
		if(ctx.sessionAttribute("user") != null) {
			User sessUser = (User)ctx.sessionAttribute("user");
			Reimbursement reimb = rServe.getReimbursement(Integer.parseInt(ctx.pathParam("r_id")));
			if(sessUser.getUserId()==reimb.getAuthorId() || sessUser.getUserRole()==2) {
				System.out.println(reimb);
				ctx.json(reimb);
				ctx.status(200);				
			} else {
				log.warn("Unauthorized User "+sessUser.getUsername()+" attempted to access sensitive files");
				ctx.result("You Do Not Have access to this Reimbursement");
			}
		}else {
			System.out.println("no user session");
			ctx.result("problem encountered");
		}
	};
	
	public Handler getReimbByUser = (ctx) ->{
		User uLoggedIn = (User)ctx.sessionAttribute("user");
		if(uLoggedIn.getUserId()==Integer.parseInt(ctx.pathParam("u_id")) || uLoggedIn.getUserRole()==2){
			
			List<Reimbursement> reimbList = rServe.getReimbByUser(Integer.parseInt(ctx.pathParam("u_id")));
			ctx.json(reimbList);
		} else {
			log.warn("Unauthorized User "+uLoggedIn.getUsername()+" attempted to access sensitive files");
			ctx.result("You do not have access to this file");
		}
	};
	
	public Handler getReimbsByStatus = (ctx) ->{
		User sessUser = (User)ctx.sessionAttribute("user");
		if(sessUser.getUserRole()==2) {
			List<Reimbursement> reimbList = rServe.getReimbsByStatus(Integer.parseInt(ctx.pathParam("s_id")));
			ctx.json(reimbList);
		} else {
			log.warn("Unauthorized User "+sessUser.getUsername()+" attempted to access sensitive files");
			ctx.result("You do not have access to this file");
		}
	};
	
	public Handler reimbGetAll = (ctx) ->{
		User sessUser = (User)(ctx.sessionAttribute("user"));
		if(sessUser != null) {
			if((sessUser).getUserRole()==2) {
				List<Reimbursement> reimbList = rServe.getAllReimbursements();
				ctx.json(reimbList);				
			} else {
				
			}
		}else {
			ctx.result("no session found");
		}
	};
	
	public Handler postReimbursement = (ctx) ->{
		User user = (User)ctx.sessionAttribute("user");
		String username = user.getUsername();
		if(username!=null) {
			rServe.createReimb(Integer.parseInt(ctx.formParam("amount")), user.getUserId(), ctx.formParam("reimbDesc"), Integer.parseInt(ctx.formParam("reimbType")));
			System.out.println("successfully created reimbursement request");
			ctx.redirect("/html/reimbcreated.html");
		}else {
			log.error("Reimbursement Request was unable to be created");
			System.out.println("Unable to create Reimbursement Request");
			ctx.redirect("/html/reimbfailed.html");
		}
	};
	
	public Handler putReimbursementApprove = (ctx) ->{
		User user = (User)ctx.sessionAttribute("user");
		if(user.getUserRole()==2) {
			rServe.approveReimb(Integer.parseInt(ctx.pathParam("r_id")));
			System.out.println("controller reached");
		}else {
			ctx.result("unable to change status");
			ctx.redirect("/html/home.html");
		}
	};
	
	public Handler putReimbursementDeny = (ctx) ->{
		User user = (User)ctx.sessionAttribute("user");
		if(user.getUserRole()==2) {
			rServe.denyReimb(Integer.parseInt(ctx.pathParam("r_id")));
		}else {
			ctx.result("unable to change status");
		}
	};
	
	public Handler putReimbursementResolve = (ctx) ->{
		User user = (User)ctx.sessionAttribute("user");
		if(user.getUserRole()==2) {
			rServe.resolveReimb(Integer.parseInt(ctx.pathParam("r_id")), user.getUserId());
		}
	};
	
	public ReimbursementController() {
		
	}

	public ReimbursementController(ReimbursementService rServe) {
		super();
		this.rServe = rServe;
	}
	
}
