package com.ers.model;

import java.sql.Timestamp;

public class Reimbursement {
	
	private int reimbId;
	private int amount;
	private int authorId;
	private int resolverId;
	private Timestamp reimbSubmitted;
	private Timestamp reimbResolved;
	private String reimbDescription;
	private int reimbStatus;
	private int reimbType;
	
	public Reimbursement(int reimbId, int amount, int authorId, int resolverId, Timestamp reimbSubmitted, Timestamp reimbResolved, String reimbDescription, int reimbType, int reimbStatus) {
		super();
		this.reimbId = reimbId;
		this.amount = amount;
		this.authorId = authorId;
		this.reimbSubmitted = reimbSubmitted;
		this.reimbResolved = reimbResolved;
		this.reimbDescription = reimbDescription;
		this.reimbType = reimbType;
		this.reimbStatus = reimbStatus;
	}
	
	public Reimbursement(int amount, int authorId, int resolverId, Timestamp reimbSubmitted, Timestamp reimbResolved, String reimbDescription, int reimbType, int reimbStatus) {
		super();
		this.amount = amount;
		this.authorId = authorId;
		this.reimbSubmitted = reimbSubmitted;
		this.reimbResolved = reimbResolved;
		this.reimbDescription = reimbDescription;
		this.reimbType = reimbType;
		this.reimbStatus = reimbStatus;
	}
	

	public Reimbursement() {
		
	}
	
	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getReimbId() {
		return reimbId;
	}


	public int getAuthorId() {
		return authorId;
	}

	public void setAuthorId(int authorId) {
		this.authorId = authorId;
	}

	public int getResolverId() {
		return resolverId;
	}

	public void setResolverId(int resolverId) {
		this.resolverId = resolverId;
	}

	public Timestamp getReimbSubmitted() {
		return reimbSubmitted;
	}

	public void setReimbSubmitted(Timestamp reimbSubmitted) {
		this.reimbSubmitted = reimbSubmitted;
	}

	public Timestamp getReimbResolved() {
		return reimbResolved;
	}

	public void setReimbResolved(Timestamp reimbResolved) {
		this.reimbResolved = reimbResolved;
	}

	public int getReimbStatus() {
		return reimbStatus;
	}
	
	public void setReimbStatus(int reimbStatus) {
		this.reimbStatus = reimbStatus;
	}

	public String getReimbDescription() {
		return reimbDescription;
	}

	public void setReimbDescription(String reimbDescription) {
		this.reimbDescription = reimbDescription;
	}
	
	public int getReimbType() {
		return reimbType;
	}

	public void setReimbType(int reimbType) {
		this.reimbType = reimbType;
	}

	@Override
	public String toString() {
		return "Reimbursement [reimbId=" + reimbId + ", authorId=" + authorId + ", resolverId=" + resolverId
				+ ", reimbSubmitted=" + reimbSubmitted + ", reimbResolved=" + reimbResolved + ", reimbStatus="
				+ reimbStatus + ", reimbType=" + reimbType + ", reimbDescription=" + reimbDescription + "]";
	}


	
}
