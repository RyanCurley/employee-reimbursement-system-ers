package com.ers.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ReimbursementDBConnection {
	
	private String url = "jdbc:mariadb://database-1.c3gfygi1xnts.us-east-2.rds.amazonaws.com:3306/ersdb";
	private String username = "ersuser";
	private String password = "mypassword";
	
	public Connection getDBConnection() throws SQLException {
		return DriverManager.getConnection(url, username, password);
	}
	
}
