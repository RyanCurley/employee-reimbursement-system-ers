package com.ers.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.ers.model.User;

public class UserDao {

	private ReimbursementDBConnection rdc;
	
	public User findByUsername(String username) {
		User user = null;
		
		try(Connection con = rdc.getDBConnection()){
			
			String sql = "SELECT * FROM ers_users WHERE ers_username=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			
			if(!rs.first()) {
				return user;
			}

			user = new User(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7));
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		return user;
	}

	public User findById(int id) {
		
		User user = null;
		
		try(Connection con = rdc.getDBConnection()){
			
			String sql = "SELECT * FROM ers_users WHERE user_id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			if(!rs.first()) {
				return user;
			}
			
			user = new User(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7));

		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		return user;
	}
	
	public void insertUser(int u_role, String username, String password, String fname, String lname, String email) {
		
		try(Connection con = rdc.getDBConnection()) {
			String sql = "CALL insert_user(?,?,?,?,?,?);";
			CallableStatement cs = con.prepareCall(sql);
			
			cs.setInt(1, u_role);
			cs.setString(2, username);
			cs.setString(3, password);
			cs.setString(4, fname);
			cs.setString(5, lname);
			cs.setString(6, email);
			
			cs.execute();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
	}

	public UserDao() {
		
	}
	
	public UserDao(ReimbursementDBConnection rdc) {
		this.rdc = rdc;
	}

}
