package com.ers.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ers.model.Reimbursement;

public class ReimbursementDao {

	private ReimbursementDBConnection rdc;

	public void insertReimbursement(int amount, int authorId, String reimbDesc, int reimbType) {
		try (Connection con = rdc.getDBConnection()) {

			String sql = "CALL insert_reimb(?,?,NULL,CURRENT_TIMESTAMP,0,?,?,0)";
			CallableStatement cs = con.prepareCall(sql);
			
			cs.setInt(1, authorId);
			cs.setInt(2, amount);
			cs.setString(3, reimbDesc);
			cs.setInt(4, reimbType);

			int status = cs.executeUpdate();
			System.out.println("Callable Statement returns: " + status);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Reimbursement findById(int id) {
		
		Reimbursement reimb = null;
		
		try(Connection con = rdc.getDBConnection()){
			
			String sql = "SELECT * FROM ers_reimbursements WHERE reimb_id = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			if(!rs.first()) {
				return reimb;
			}
			
			reimb = new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getTimestamp(5), rs.getTimestamp(6), rs.getString(7), rs.getInt(8), rs.getInt(9));

		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		return reimb;
	}
	
	public List<Reimbursement> getAllReimbByUser(int userId) {

		List<Reimbursement> reimbList = new ArrayList<>();
		
		try(Connection con = rdc.getDBConnection()){
			
			String sql = "SELECT * FROM ers_reimbursements WHERE author_id = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, userId);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				reimbList.add(new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getTimestamp(5), rs.getTimestamp(6), rs.getString(7), rs.getInt(8), rs.getInt(9)));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		return reimbList;
	}
	
	public List<Reimbursement> getAllReimb() {

		List<Reimbursement> reimbList = new ArrayList<>();
		
		try(Connection con = rdc.getDBConnection()){
			
			String sql = "SELECT * FROM ers_reimbursements";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				reimbList.add(new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getTimestamp(5), rs.getTimestamp(6), rs.getString(7), rs.getInt(8), rs.getInt(9)));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		return reimbList;
	}
	
	public void approveReimb(int reimbId) {

		try (Connection con = rdc.getDBConnection()) {
			String sql = "UPDATE ers_reimbursements SET reimb_status = 2 where reimb_id = ?;";

			PreparedStatement ps = con.prepareStatement(sql);

			ps.setInt(1, reimbId);

			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public void denyReimb(int reimbId) {

		try (Connection con = rdc.getDBConnection()) {
			String sql = "UPDATE ers_reimbursements SET reimb_status = 1 WHERE reimb_id = ?;";

			PreparedStatement ps = con.prepareStatement(sql);

			ps.setInt(1, reimbId);

			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	public void resolveReimb(int reimbId, int resolverId) {
		try(Connection con = rdc.getDBConnection()){
			String sql = "UPDATE ers_reimbursements SET reimb_resolved = CURRENT_TIMESTAMP, resolver_id=? WHERE reimb_id = ?;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, resolverId);
			ps.setInt(2, reimbId);
			ps.executeUpdate();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public List<Reimbursement> getAllReimbByStatus(int reimbStatus) {

		List<Reimbursement> reimbList = new ArrayList<>();
		
		try(Connection con = rdc.getDBConnection()){
			
			String sql = "SELECT * FROM ers_reimbursements WHERE reimb_status = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, reimbStatus);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				reimbList.add(new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getTimestamp(5), rs.getTimestamp(6), rs.getString(7), rs.getInt(8), rs.getInt(9)));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		return reimbList;
	}
	
	
	public ReimbursementDao() {
		
	}
	
	public ReimbursementDao(ReimbursementDBConnection rdc) {
		this.rdc = rdc;
	}
	
}
