/**
 * 
 */

window.onload=function(){
	console.log("js linked");
	getSessionUser();
}

function getSessionUser(){
	let xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function(){
		if(xhttp.readyState==4 && xhttp.status==200){
			try{
				user = JSON.parse(xhttp.responseText)
			} catch (Exception) {
				window.alert("User Logged Out");
				window.location.replace("http://localhost:9100/html/login.html");
			};
			console.log(user);
			checkUser(user);
			greetUser(user);
		}
	}
	
	xhttp.open("GET", "http://localhost:9100/users/session");
	xhttp.send();
};

function greetUser(user) {
	let heading = document.getElementById("welcome");
	heading.innerText = `Welcome, ${user.firstName}!`
};

function checkUser(user) {
	if(user==null) {
		window.alert("You Do Not Have Access To This Page");
		window.location.replace("http://localhost:9100/html/login.html");
	}
};