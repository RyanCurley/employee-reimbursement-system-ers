/**
 * 
 */
window.onload=function(){
	console.log('js linked');
	getSessionUser();
	getReimbursements();
};

function getSessionUser(){
	let xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function(){
		if(xhttp.readyState==4 && xhttp.status==200){
			try{
				user = JSON.parse(xhttp.responseText)
			} catch (Exception) {
				window.alert("User Logged Out");
				window.location.replace("http://localhost:9100/html/login.html");
			};
			console.log(user);
			checkUser(user);
			
		}
	}
	
	xhttp.open("GET", "http://localhost:9100/users/session");
	xhttp.send();
};

function checkUser(user) {
	if(user.userRole!=2) {
		window.alert("You Do Not Have Access To This Page");
		window.location.replace("http://localhost:9100/html/home.html");
	}
};

function getReimbursements(){	
	let xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function() {
		console.log("ready state has changed");
		
		if(xhttp.readyState==4 && xhttp.status==200){
			console.log(xhttp.responseText);
			let reimbs = JSON.parse(xhttp.responseText);

			loadTableData(reimbs);

		}
	}
	
	xhttp.open("GET", `http://localhost:9100/reimbursements/all`);
	
	xhttp.send();
};

const table = document.querySelector("#reimb-table > tbody");

function loadTableData(reimbs) {

	console.log(reimbs);

	reimbs.forEach((row)=>{
		const tr = document.createElement("tr");
		Object.values(row).forEach((cell) =>{
			const td = document.createElement("td");
			td.textContent = cell;
			tr.appendChild(td);
		});
		
		table.appendChild(tr);
	});
};

/*function correctTableData(table) {
	for (let r=0, n=table.rows.length; r<n; r++){
		if (table.rows[r].cells[7].innerText>1000000){
			table.rows[r].cells[7].innerText= new Date(table.rows[r].cells[7].value);
		} else if (table.rows[r].cells[7].innerText==null){
			table.rows[r].cells[7].innerText = "Unresolved";
		}
	}
}*/

