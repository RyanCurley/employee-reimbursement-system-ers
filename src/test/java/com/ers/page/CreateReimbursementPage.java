package com.ers.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CreateReimbursementPage {

	private WebDriver driver;
	private WebElement amountField;
	private WebElement descriptionField;
	private WebElement typeDropdown;
	private WebElement foodOption;
	private WebElement submit;
	
	public CreateReimbursementPage(WebDriver driver) {
		this.driver = driver;
		
		this.amountField = driver.findElement(By.id("reimbAmount"));
		this.descriptionField = driver.findElement(By.name("reimbDesc"));
		this.typeDropdown = driver.findElement(By.name("reimbType"));
		this.foodOption = driver.findElement(By.id("foodOption"));
		this.submit = driver.findElement(By.name("reimbSubmit"));
	}
	
	public void setAmount(String amount) {
		this.amountField.clear();
		this.amountField.sendKeys(amount);
	}
	
	public void setDescription(String description) {
		this.descriptionField.clear();
		this.descriptionField.sendKeys(description);
	}
	
	public void clickDropdown() {
		this.typeDropdown.click();
	}
	
	public void clickFoodOption() {
		this.foodOption.click();
	}
	
	public void clickSubmit() {
		this.submit.click();
	}
}
