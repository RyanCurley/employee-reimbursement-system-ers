package com.ers.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {

	private WebDriver driver;
	private WebElement usernameField;
	private WebElement passwordField;
	private WebElement submitButton;
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
		this.navigateTo();
		
		this.usernameField = driver.findElement(By.id("username"));
		this.passwordField = driver.findElement(By.id("password"));
		this.submitButton = driver.findElement(By.id("userSubmit"));
	}
	
	public void setUsername(String username) {
		this.usernameField.clear();
		this.usernameField.sendKeys(username);
	}
	
	public String getUsername() {
		return this.usernameField.getAttribute("value");
	}
	
	public void setPassword(String password) {
		this.passwordField.clear();
		this.passwordField.sendKeys(password);
	}
	
	public String getPassword() {
		return this.passwordField.getAttribute("value");
	}
	
	public void submit() {
		this.submitButton.click();
	}
	
	public void navigateTo() {
		this.driver.get("http://localhost:9100/html/login.html");
	}
	
}
