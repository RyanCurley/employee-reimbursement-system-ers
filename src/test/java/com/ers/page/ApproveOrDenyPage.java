package com.ers.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ApproveOrDenyPage {
	
	private WebDriver driver;
	private WebElement idField;
	private WebElement searchButton;
	private WebElement approveButton;
	
	public ApproveOrDenyPage(WebDriver driver) {
		this.driver = driver;
		
		this.idField = driver.findElement(By.id("reimbId"));
		this.searchButton = driver.findElement(By.id("reimbSubmit"));
		this.approveButton = driver.findElement(By.id("approve"));
		
	}
	
	public void setIdField(String id) {
		this.idField.clear();
		this.idField.sendKeys(id);
	}
	
	public void searchClick() {
		this.searchButton.click();
	}
	
	public void approveClick() {
		this.approveButton.click();
	}
	
}
