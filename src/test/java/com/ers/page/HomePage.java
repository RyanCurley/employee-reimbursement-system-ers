package com.ers.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage {

	private WebDriver driver;
	private WebElement dropDown;
	private WebElement createReimb;
	private WebElement approveOrDeny;
	
	public HomePage(WebDriver driver) {
		this.driver = driver;
		
		this.dropDown = driver.findElement(By.id("navbarDropdown"));
		this.createReimb = driver.findElement(By.id("createReimbursement"));
		this.approveOrDeny = driver.findElement(By.id("approveOrDeny"));
	}

	public void dropDownClick() {
		this.dropDown.click();
	}
	
	public void createReimbClick() {
		this.createReimb.click();
	}
	
	public void approveOrDenyClick() {
		this.approveOrDeny.click();
	}
	
}