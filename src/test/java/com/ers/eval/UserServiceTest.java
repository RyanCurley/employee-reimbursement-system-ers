package com.ers.eval;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.ers.dao.UserDao;
import com.ers.model.User;
import com.ers.service.UserService;

public class UserServiceTest {
	
	@Mock
	private UserDao mockedDao;
	private UserService testService = new UserService(mockedDao);
	private User testUser;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);
		testService = new UserService(mockedDao);
		testUser = new User(2, 2,"charlie", "pass", "Charlie", "Kelly", "charlie@rev");
		when(mockedDao.findByUsername("charlie")).thenReturn(testUser);
		
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testGetUserByUsernameSuccess() {
		assertEquals(testService.getUser("charlie"), testUser);
	}
	
	@Test
	public void testUserVerify() {
		assertEquals(testService.userVerify("charlie", "pass"), true);
	}
	
}
