package com.ers.eval;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.ers.dao.ReimbursementDao;
import com.ers.model.Reimbursement;
import com.ers.service.ReimbursementService;

public class ReimbursementServiceTest {

	@Mock
	private ReimbursementDao mockedDao;
	private ReimbursementService testService = new ReimbursementService(mockedDao);
	private Reimbursement testReimb;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);
		testService = new ReimbursementService(mockedDao);
		testReimb = new Reimbursement(5, 100, 1, new Timestamp(2021,01,9,23,56,56,0), null, "MONEY", 1,1);
		when(mockedDao.findById(5)).thenReturn(testReimb);
		
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testGetReimbByIdSuccess() {
		assertEquals(testService.getReimbursement(5), testReimb);
	}
	
}
