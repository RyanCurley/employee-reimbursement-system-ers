package com.ers.eval;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.ers.page.ApproveOrDenyPage;
import com.ers.page.CreateReimbursementPage;
import com.ers.page.HomePage;
import com.ers.page.LoginPage;

public class SeleniumDriver {

	public static void main(String[] args) throws InterruptedException {
		
		File file = new File("src/test/resources/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		
		WebDriver driver = new ChromeDriver();
		
		LoginPage loginPage = new LoginPage(driver);
		
		loginPage.setUsername("ryan");
		loginPage.setPassword("pass");
		loginPage.submit();
		
		TimeUnit.SECONDS.sleep(1);
		HomePage homePage = new HomePage(driver);
		
		homePage.dropDownClick();
		homePage.createReimbClick();
				
		TimeUnit.SECONDS.sleep(1);
		CreateReimbursementPage createReimbPage = new CreateReimbursementPage(driver);
		
		createReimbPage.setAmount("50");
		createReimbPage.setDescription("Dinner With a Client");
		createReimbPage.clickDropdown();
		createReimbPage.clickFoodOption();
		createReimbPage.clickSubmit();
		
		TimeUnit.SECONDS.sleep(2);
		
		LoginPage loginPage2 = new LoginPage(driver);
		loginPage2.setUsername("charlie");
		loginPage2.setPassword("pass");
		loginPage2.submit();
		
		TimeUnit.SECONDS.sleep(1);
		
		HomePage homePage2 = new HomePage(driver);
		
		homePage2.dropDownClick();
		homePage2.approveOrDenyClick();
		
		TimeUnit.SECONDS.sleep(1);
		
		ApproveOrDenyPage aodPage = new ApproveOrDenyPage(driver);
		
		aodPage.setIdField("1");
		aodPage.searchClick();
		
		TimeUnit.SECONDS.sleep(1);
		
		aodPage.approveClick();
		
	}
	
}
