package com.ers.eval;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.ers.dao.ReimbursementDBConnection;
import com.ers.dao.ReimbursementDao;
import com.ers.model.Reimbursement;

public class ReimbursementDaoTest {
	
	@Mock
	private ReimbursementDBConnection rdc;
	
	@Mock 
	private Connection c;
	
	@Mock
	private PreparedStatement ps;
	
	@Mock
	private ResultSet rs;
	
	private Reimbursement testReimb;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(rdc.getDBConnection()).thenReturn(c);
		when(c.prepareStatement(any(String.class))).thenReturn(ps);
		testReimb = new Reimbursement(5, 100, 1, new Timestamp(2021,01,9,23,56,56,0), null, "MONEY", 1,1);
		when(rs.first()).thenReturn(true);
		when(rs.getInt(1)).thenReturn(testReimb.getReimbId());
		when(rs.getInt(2)).thenReturn(testReimb.getAuthorId());
		when(rs.getTimestamp(3)).thenReturn(testReimb.getReimbSubmitted());
		when(rs.getTimestamp(4)).thenReturn(testReimb.getReimbResolved());
		when(rs.getString(5)).thenReturn(testReimb.getReimbDescription());
		when(rs.getInt(6)).thenReturn(testReimb.getReimbType());
		when(rs.getInt(7)).thenReturn(testReimb.getReimbStatus());
		when(ps.executeQuery()).thenReturn(rs);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testFindByIdSuccess() {
		assertEquals(new ReimbursementDao(rdc).findById(5).getReimbId(), testReimb.getReimbId());
	}
		
}
