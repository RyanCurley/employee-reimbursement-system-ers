package com.ers.eval;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.ers.dao.ReimbursementDBConnection;
import com.ers.dao.UserDao;
import com.ers.model.User;

public class UserDaoTest {
	
	@Mock
	private ReimbursementDBConnection rdc;
	
	@Mock 
	private Connection c;
	
	@Mock
	private PreparedStatement ps;
	
	@Mock
	private ResultSet rs;
	
	private User testUser;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(rdc.getDBConnection()).thenReturn(c);
		when(c.prepareStatement(any(String.class))).thenReturn(ps);
		testUser = new User(2, 2,"ryanc", "pass", "ryan", "curley", "ryan@rev");
		when(rs.first()).thenReturn(true);
		when(rs.getInt(1)).thenReturn(testUser.getUserId());
		when(rs.getInt(2)).thenReturn(testUser.getUserRole());
		when(rs.getString(3)).thenReturn(testUser.getUsername());
		when(rs.getString(4)).thenReturn(testUser.getPassword());
		when(rs.getString(5)).thenReturn(testUser.getFirstName());
		when(rs.getString(6)).thenReturn(testUser.getLastName());
		when(rs.getString(7)).thenReturn(testUser.getEmail());
		when(ps.executeQuery()).thenReturn(rs);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testFindByIdSuccess() {
		assertEquals(new UserDao(rdc).findById(2).getUserId(), testUser.getUserId());
	}
	
	@Test
	public void testFindByUsernameSuccess() {
		assertEquals(new UserDao(rdc).findByUsername("ryanc").getUsername(), testUser.getUsername());
	}
	
}
