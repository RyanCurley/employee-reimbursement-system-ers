package com.ers.selenium;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.ers.page.CreateReimbursementPage;
import com.ers.page.HomePage;
import com.ers.page.LoginPage;

public class E2ETest {
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	@Before
	public void setUp() throws Exception {
		
	}
	
	@After
	public void tearDown() throws Exception {
		RunEnvironment.driver.quit();
	}
	
	@Test
	public void testSubmitReimbursementRequest() {
		WebDriver driver = RunEnvironment.getWebDriver();
		
		LoginPage loginPage = new LoginPage(driver);
		
		loginPage.setUsername("ryan");
		loginPage.setPassword("pass");
		loginPage.submit();
		
		HomePage homePage = new HomePage(driver);
		
		homePage.dropDownClick();
		homePage.createReimbClick();
		
//		TimeUnit.SECONDS.sleep(1);
		CreateReimbursementPage createReimbPage = new CreateReimbursementPage(driver);
		
		createReimbPage.setAmount("50");
		createReimbPage.setDescription("Dinner With a Client");
		createReimbPage.clickDropdown();
		createReimbPage.clickFoodOption();
		createReimbPage.clickSubmit();
		
		String testElement = driver.findElement(By.id("success")).getText();
		assertEquals(testElement, "Success!");
		}
	
}
