package com.ers.selenium;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class EnvironmentManager {

	public static WebDriver driver;
	
	public static void initWebDriver() {
		File file = new File("src/test/resources/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		WebDriver driver = new ChromeDriver();
		RunEnvironment.setWebDriver(driver);
		
	}
	
	public static void shutDownDriver() {
		RunEnvironment.getWebDriver().quit();
	}
	
}
