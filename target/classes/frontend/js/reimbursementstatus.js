/**
 * 
 */
window.onload=function(){
	console.log('js linked');
	getSessionUser();
	document.getElementById('searchBtn').addEventListener('click', getReimbursements);
};

function getSessionUser(){
	let xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function(){
		if(xhttp.readyState==4 && xhttp.status==200){
			try{
				user = JSON.parse(xhttp.responseText)
			} catch (Exception) {
				window.alert("User Logged Out");
				window.location.replace("http://localhost:9100/html/login.html");
			};
			console.log(user);
			checkUser(user);
			
		}
	}
	
	xhttp.open("GET", "http://localhost:9100/users/session");
	xhttp.send();
};

function checkUser(user) {
	if(user.userRole!=2) {
		window.alert("You Do Not Have Access To This Page");
		window.location.replace("http://localhost:9100/html/home.html");
	}
};


function getReimbursements(){	
let status = document.getElementById('status').value;

console.log(status);
	let xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function() {
		console.log("ready state has changed");
		
		if(xhttp.readyState==4 && xhttp.status==200){
//			console.log(xhttp.responseText);
			let reimbs = JSON.parse(xhttp.responseText);

			loadTableData(reimbs);
		}
	}
	
	xhttp.open("GET", `http://localhost:9100/reimbursements/approval/${status}`);
	
	xhttp.send();
};

const table = document.querySelector("#reimb-table > tbody");

function loadTableData(reimbs) {

//	console.log(reimbs);

	while(table.firstChild){
		table.removeChild(table.firstChild);
	}

	reimbs.forEach((row)=>{
		const tr = document.createElement("tr");
			Object.values(row).forEach((cell) =>{
				const td = document.createElement("td");
				td.textContent = cell;
				tr.appendChild(td);
				});
		table.appendChild(tr);			

	});
};