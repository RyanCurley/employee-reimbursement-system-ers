/**
 * 
 */
window.onload=function(){
	console.log('js linked');
	getSessionUser();
	document.getElementById('reimbSubmit').addEventListener('click', getReimbursement);
};

function getSessionUser(){
	let xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function(){
		if(xhttp.readyState==4 && xhttp.status==200){
			try{
				user = JSON.parse(xhttp.responseText)
			} catch (Exception) {
				window.alert("User Logged Out");
				window.location.replace("http://localhost:9100/html/login.html");
			};
			console.log(user);
			checkUser(user);
			greetUser(user);
		}
	}
	
	xhttp.open("GET", "http://localhost:9100/users/session");
	xhttp.send();
};

function getReimbursement(){
	let reimbId = document.getElementById("reimbId").value;
	console.log(reimbId);
	
	let xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function() {
		console.log("ready state has changed");
		
		if(xhttp.readyState==4 && xhttp.status==200){
			console.log(xhttp.responseText);
			let reimb = JSON.parse(xhttp.responseText);
			console.log(reimb);
			loadTableData(reimb);
		}
	}
	
	xhttp.open("GET", `http://localhost:9100/reimbursements/${reimbId}/view`);
	
	xhttp.send();
};

function loadTableData(reimb) {
	document.getElementById("r_id").innerText=(reimb.reimbId);
	document.getElementById("amount").innerText=(reimb.amount);
	document.getElementById("auth").innerText=(reimb.authorId);
	document.getElementById("resolver").innerText=(reimb.resolverId);
	document.getElementById("type").innerText=(reimb.reimbType);
	document.getElementById("desc").innerText=(reimb.reimbDescription);
	document.getElementById("submitted").innerText=(new Date(reimb.reimbSubmitted));
	
	if(reimb.Resolved==null){
		document.getElementById("resolved").innerText="Unresolved";
	} else{
		document.getElementById("resolved").innerText=(new Date(reimb.reimbResolved));
	}
	document.getElementById("status").innerText=(reimb.reimbStatus);
};